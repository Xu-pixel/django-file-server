# 关于
django 静态文件服务器

# 部署
虚拟环境
```shell
python -m venv venv
```

激活虚拟环境
```shell
# linux
source ./venv/bin/activate

# windows
./venv/Scripts/Activate.ps1
```

安装依赖
```shell

pip install -r requirements.txt
```

把文件放到media文件夹下即可

启动

```shell
gunicorn -w 4 -b 0.0.0.0:8000 fileserver.wsgi
```

手动关掉
```shell
ps -elf | grep gunicorn

kill -9 <gunicorn:id>
```