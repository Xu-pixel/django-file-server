from django.http import  HttpResponseNotFound
from django.shortcuts import render
import os
# Create your views here.

def getFile(request):
    path_arr = request.path.split('/')
    def concate(*p):
        return os.path.join(os.getcwd(),'media',*p)

    try:
        files = os.listdir(concate(*path_arr))
    except:
        return HttpResponseNotFound('404')

    files = [(file,os.path.isdir(concate(*path_arr,file))) for file in files]
    files.sort(key=lambda x:-x[1])
    ctx = {'files':files,'path_arr':path_arr[1:],'path_url':request.path[1:]}
    return render(request,'index.html',ctx)